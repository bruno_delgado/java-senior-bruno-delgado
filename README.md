
# Employee Rest Application

A Spring Boot Rest Application made to a Technical Avaliation for the Mobicare company. In this project you will see:

- Java 11
- Swagger
- Spring-Boot
- Dockerfile
- Docker Compose
- Schema Migrations
- JUnit Jupiter (Unit Tests and Integration Tests)

## Development Pre-Requisites
- **IDE**: to develop and debug the application
- **Java 11**: the programming language for this project is Java
- **Maven**: used to manage packages and build the project
- **Docker**: to avoid configuring development environment
- **Docker-Compose**: to create the infrastructure

## Running using Docker

This project contains a Dockerfile / Docker Compose that creates all the required infraestructure to develop and test the project:

- **PostgreSQL**: database used to load the data extracted from Kafka;
- **Employee Rest Application**: the application itself.


**1. Build the JAR**  
`$ mvn clean install`

**2. Start-Up Containers**  
`$ docker-compose up`  

**3. Shutdown Containers**  
`$ docker-compose down`


#### Build Note

When you run the project using Docker, it compiles the source code and creates
a target folder containing the project's jar. After that you will not be able
to compile the project using your IDE or your command line because the
folder's owner now is Docker's user.

To solve this problem, just delete the target folder using sudo or
administrative privileges when you want to run the application using your
IDE or command line.

## Running at local machine

This project contains a maven plugin to execute the application.
If you want to run it locally it is important that you guarantee
that there's a PostgreSQL running in localhost.

    $ mvn clean install spring-boot:run -Dspring-boot.run.profiles=local

## Swagger

When the project is running you can see all available endpoints at http://localhost:8080/swagger-ui.html.

Enjoy it :)
