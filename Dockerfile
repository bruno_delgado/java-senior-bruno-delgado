FROM maven:3.8.2-jdk-11 AS BUILD

COPY ./target/employee.jar ./employee.jar

EXPOSE 8080

CMD ["java", "-jar", "/employee.jar", "--spring.profiles.active=docker"]