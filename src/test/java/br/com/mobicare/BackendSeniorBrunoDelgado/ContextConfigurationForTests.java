package br.com.mobicare.BackendSeniorBrunoDelgado;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.BlacklistService;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.LoadService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.vendor.Database;

import javax.sql.DataSource;

import static org.mockito.Mockito.mock;

@TestConfiguration
public class ContextConfigurationForTests {

    @Bean
    public Database database() {
        return Database.H2;
    }

    @Bean
    @Profile("!blacklist-test")
    public BlacklistService blacklistService() {
        return mock(BlacklistService.class);
    }

}
