package br.com.mobicare.BackendSeniorBrunoDelgado.utils;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.ContactInformation;

public class ContactInformationBuilder {

    private String email;
    private String phone;

    public ContactInformationBuilder() {
        this.email = "brunodelgado@mail.com";
        this.phone = "067999999999";
    }

    public ContactInformation build() {
        return new ContactInformation(email, phone);
    }
}
