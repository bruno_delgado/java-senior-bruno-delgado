package br.com.mobicare.BackendSeniorBrunoDelgado.utils;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;

public class SectorBuilder {
    private String name;

    public SectorBuilder() {
        this.name = "Software Architecture";
    }

    public SectorBuilder with(String name) {
        this.name = name;
        return this;
    }

    public Sector build() {
        return new Sector(this.name);
    }
}
