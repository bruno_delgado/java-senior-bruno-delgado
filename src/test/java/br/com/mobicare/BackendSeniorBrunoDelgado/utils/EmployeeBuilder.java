package br.com.mobicare.BackendSeniorBrunoDelgado.utils;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.ContactInformation;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.PersonalInformation;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;

public class EmployeeBuilder {
    private Sector sector;
    private ContactInformation contactInformation;
    private PersonalInformation personalInformation;

    public EmployeeBuilder() {
        this.sector = new SectorBuilder().build();
        this.contactInformation = new ContactInformationBuilder().build();
        this.personalInformation = new PersonalInformationBuilder().build();
    }

    public EmployeeBuilder with(PersonalInformation personalInformation) {
        this.personalInformation = personalInformation;
        return this;
    }

    public EmployeeBuilder with(Sector sector) {
        this.sector = sector;
        return this;
    }

    public Employee build() {
        return new Employee(personalInformation, contactInformation, sector);
    }
}
