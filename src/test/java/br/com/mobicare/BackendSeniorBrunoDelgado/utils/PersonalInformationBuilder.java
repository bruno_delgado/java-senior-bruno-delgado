package br.com.mobicare.BackendSeniorBrunoDelgado.utils;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.DateOfBirth;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.PersonalInformation;

import java.time.LocalDate;

public class PersonalInformationBuilder {
    private String cpf;
    private String name;
    private DateOfBirth dateOfBirth;

    public PersonalInformationBuilder() {
        this.cpf = "17278265908";
        this.name = "Bruno Delgado";
        this.dateOfBirth = new DateOfBirth(1988, 11, 30);
    }

    public PersonalInformationBuilder legalAge() {
        this.dateOfBirth = new DateOfBirth(1988, 11, 30);
        return this;
    }

    public PersonalInformationBuilder underage() {
        final var now = LocalDate.now();
        final var oldYear = now.getYear() - 17;
        final var underage = LocalDate.of(oldYear, now.getMonth(), now.getDayOfMonth());
        this.dateOfBirth = new DateOfBirth(underage.getYear(), underage.getMonth().getValue(), underage.getDayOfMonth());
        return this;
    }

    public PersonalInformationBuilder elderly() {
        final var now = LocalDate.now();
        final var oldYear = now.getYear() - 66;
        final var underage = LocalDate.of(oldYear, now.getMonth(), now.getDayOfMonth());
        this.dateOfBirth = new DateOfBirth(underage.getYear(), underage.getMonth().getValue(), underage.getDayOfMonth());
        return this;
    }

    public PersonalInformationBuilder with(DateOfBirth dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public PersonalInformationBuilder withCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public PersonalInformation build() {
        return new PersonalInformation(this.cpf, this.name, this.dateOfBirth);
    }
}
