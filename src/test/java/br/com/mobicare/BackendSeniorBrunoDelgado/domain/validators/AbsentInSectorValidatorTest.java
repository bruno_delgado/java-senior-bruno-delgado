package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validators;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.AbsentInSectorValidator;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.EmployeeValidationResult;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Test Cases for Absent in Sector Validations")
public class AbsentInSectorValidatorTest {

    private Employee employee;

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private AbsentInSectorValidator absentInSectorValidator;

    @BeforeEach
    public void setUp() {
        employee = new EmployeeBuilder().build();
    }

    @Test
    @DisplayName("Should validate the employee is not present in any sector")
    public void testEmployeeDoesNotBelongsToSector() {
        final EmployeeValidationResult validationResult = absentInSectorValidator.validate(employee);

        assertTrue(validationResult.isValid());
    }

    @Test
    @DisplayName("Should invalidate when the employee is present in the sector")
    public void testEmployeeBelongsToSector() {
        Mockito.when(employeeRepository.findByPersonalInformation_Cpf(employee.getCpf())).thenReturn(employee);

        final EmployeeValidationResult validationResult = absentInSectorValidator.validate(employee);

        assertFalse(validationResult.isValid());
    }

    @Test
    @DisplayName("Should inform a reason when employee is invalidated")
    public void testValidationMessage() {
        Mockito.when(employeeRepository.findByPersonalInformation_Cpf(employee.getCpf())).thenReturn(employee);

        final var validationResult = absentInSectorValidator.validate(employee);

        final var sectorName = employee.getSector().getName();
        final var expectedMessage = String.format("Employee already belongs to the sector \"%s\"", sectorName);
        assertTrue(validationResult.getMessages().size() == 1);
        assertEquals(expectedMessage, validationResult.getMessages().get(0));
    }
}
