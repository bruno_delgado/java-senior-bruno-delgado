package br.com.mobicare.BackendSeniorBrunoDelgado.domain;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Test Cases for Employee")
public class EmployeeTest {

    private String cpf;
    private String name;
    private String email;
    private String phone;
    private Sector sector;
    private DateOfBirth dateOfBirth;
    private ContactInformation contactInformation;
    private PersonalInformation personalInformation;

    @BeforeEach
    public void setUp() {
        cpf = "22062406029";
        name = "Bruno Augusto";
        phone = "067992819292";
        email = "brunodelgado@email.com";
        sector = new Sector("Software Architecture");
        dateOfBirth = new DateOfBirth(1995, 2, 27);
        contactInformation = new ContactInformation(email, phone);
        personalInformation = new PersonalInformation(cpf, name, dateOfBirth);
    }

    @Test
    @DisplayName("Should create an employee with a name")
    public void testName() {
        final var expectedName = "Bruno Delgado";
        final var personalInformation = new PersonalInformation(cpf, expectedName, dateOfBirth);
        final var employee = new Employee(personalInformation, contactInformation, sector);

        assertEquals(expectedName, employee.getName());
    }

    @Test
    @DisplayName("Should create an employee with a birth date")
    public void testBirthDate() {
        final var birthDate = new DateOfBirth(1988, 11, 30);
        final var personalInformation = new PersonalInformation(cpf, name, birthDate);
        final var employee = new Employee(personalInformation, contactInformation, sector);

        assertEquals(birthDate, employee.getDateOfBirth());
    }

    @Test
    @DisplayName("Should create an employee with a cpf")
    public void testCpf() {
        final var cpf = "11497926041";
        final var personalInformation = new PersonalInformation(cpf, name, dateOfBirth);
        final var employee = new Employee(personalInformation, contactInformation, sector);

        assertEquals(cpf, employee.getCpf());
    }

    @Test
    @DisplayName("Should create an employee with a sector")
    public void testSector() {
        final var sector = new Sector("Software Engineer");
        final var personalInformation = new PersonalInformation(cpf, name, dateOfBirth);
        final var employee = new Employee(personalInformation, contactInformation, sector);

        assertEquals(sector, employee.getSector());
    }

    @Test
    @DisplayName("Should create an employee with email")
    public void testEmail() {
        final var email = "brunodelgado@email.com";
        final var contactInformation = new ContactInformation(email, phone);
        final var employee = new Employee(personalInformation, contactInformation, sector);

        assertEquals(email, employee.getEmail());
    }

    @Test
    @DisplayName("Should create an employee with phone number")
    public void testPhone() {
        final var phone = "067992819292";
        final var contactInformation = new ContactInformation(email, phone);
        final var employee = new Employee(personalInformation, contactInformation, sector);

        assertEquals(phone, employee.getPhone());
    }

}
