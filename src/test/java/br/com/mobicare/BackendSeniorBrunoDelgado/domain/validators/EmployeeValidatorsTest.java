package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validators;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Blacklist;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.BlacklistService;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.LoadService;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.*;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
@DisplayName("Test Cases for Employee Validators")
public class EmployeeValidatorsTest {

    @Mock
    private BlacklistService blacklistService;

    @Mock
    private LoadService loadService;

    private Employee employee;
    private List<EmployeeValidation> employeeValidations;

    @BeforeEach
    public void setUp() {
        employeeValidations = new ArrayList<>();
        employee = new EmployeeBuilder().build();
    }

    @Test
    @DisplayName("Should validate if all validators are in a valid state")
    public void testTwoValidationsWithValidResults() {
        employeeValidations.add(new BlacklistValidator(blacklistService));
        employeeValidations.add(new MaximumUnderagePerSectorValidator(loadService));
        final var employeeValidators = new EmployeeValidators(employeeValidations);

        final EmployeeValidationResult validationResult = employeeValidators.validate(employee);

        Assertions.assertTrue(validationResult.isValid());
    }

    @Test
    @DisplayName("Should invalidate if one validator is in invalid state")
    public void testValidationsWithInvalidResults() {
        Mockito.when(blacklistService.verifyEmployee(employee)).thenReturn(Blacklist.DENIED);
        employeeValidations.add(new BlacklistValidator(blacklistService));
        employeeValidations.add(new MaximumUnderagePerSectorValidator(loadService));
        final var employeeValidators = new EmployeeValidators(employeeValidations);

        final EmployeeValidationResult validationResult = employeeValidators.validate(employee);

        Assertions.assertFalse(validationResult.isValid());
    }

    @Test
    @DisplayName("Should inform the reason when the employee is invalidated")
    public void testValidationMessage() {
        Mockito.when(blacklistService.verifyEmployee(employee)).thenReturn(Blacklist.DENIED);
        employeeValidations.add(new BlacklistValidator(blacklistService));
        employeeValidations.add(new MaximumUnderagePerSectorValidator(loadService));
        final var employeeValidators = new EmployeeValidators(employeeValidations);

        final EmployeeValidationResult validationResult = employeeValidators.validate(employee);

        final String expectedMessage = "Employee is in the Blacklist";
        assertTrue(validationResult.getMessages().size() == 1);
        assertEquals(expectedMessage, validationResult.getMessages().get(0));
    }
}
