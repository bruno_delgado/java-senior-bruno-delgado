package br.com.mobicare.BackendSeniorBrunoDelgado.domain;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.LoadByAge;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;


@DisplayName("Test Cases for Load Information")
public class LoadByAgeTest {

    @ParameterizedTest
    @MethodSource("testValues")
    @DisplayName("Should calculate the load percentage")
    public void testGetPercentage(Long conditional, Long total, Double expectedPercentage) {
        LoadByAge loadByAge = new LoadByAge(conditional, total);

        Assertions.assertEquals(expectedPercentage, loadByAge.getPercentage());
    }

    @Test
    @DisplayName("Should return a load of 0 when conditional and total is 0")
    public void testGetPercentageOfEmptyLoad() {
        LoadByAge loadByAge = new LoadByAge(0L, 0L);

        Assertions.assertEquals(0, loadByAge.getPercentage());
    }

    private static Stream<Arguments> testValues() {
        return Stream.of(
                Arguments.of(20L, 100L, 0.2),
                Arguments.of(15L, 100L, 0.15),
                Arguments.of(50L, 100L, 0.5)
        );
    }
}
