package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validators;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Blacklist;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.BlacklistService;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.BlacklistValidator;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.EmployeeValidationResult;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
@DisplayName("Test Cases for Blacklist Validators")
public class BlacklistValidatorTest {

    @Mock
    private BlacklistService blacklistService;

    @InjectMocks
    private BlacklistValidator blacklistValidator;

    private Employee employee;

    @BeforeEach
    public void setUp() {
        employee = new EmployeeBuilder().build();
    }

    @Test
    @DisplayName("Should validate who is allowed in blacklist")
    public void testNotInBlacklist() {
        Mockito.when(blacklistService.verifyEmployee(employee)).thenReturn(Blacklist.ALLOWED);

        final EmployeeValidationResult validationResult = blacklistValidator.validate(employee);

        Assertions.assertTrue(validationResult.isValid());
    }

    @Test
    @DisplayName("Should invalidate who is denied in the blacklist")
    public void testInBlackList() {
        Mockito.when(blacklistService.verifyEmployee(employee)).thenReturn(Blacklist.DENIED);

        final EmployeeValidationResult validationResult = blacklistValidator.validate(employee);

        Assertions.assertFalse(validationResult.isValid());
    }

    @Test
    @DisplayName("Should inform a reason when the employee is invalidated")
    public void testValidationMessage() {
        Mockito.when(blacklistService.verifyEmployee(employee)).thenReturn(Blacklist.DENIED);

        final EmployeeValidationResult validationResult = blacklistValidator.validate(employee);

        final String expectedMessage = "Employee is in the Blacklist";
        assertTrue(validationResult.getMessages().size() == 1);
        assertEquals(expectedMessage, validationResult.getMessages().get(0));
    }
}
