package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validators;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.LoadByAge;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.PersonalInformation;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.LoadService;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.EmployeeValidationResult;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.MaximumUnderagePerSectorValidator;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.PersonalInformationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("Test Cases for Maximum Underage Per Sector Validations")
public class MaximumUnderagePerSectorValidatorTest {

    @Mock
    private LoadService loadService;

    @InjectMocks
    private MaximumUnderagePerSectorValidator maximumUnderagePerSectorValidator;

    private Employee legalAgeEmployee;
    private Employee underAgeEmployee;

    @BeforeEach
    public void setUp() {
        underAgeEmployee = createUnderAgeEmployee();
        legalAgeEmployee = createLegalAgeEmployee();
    }

    @Test
    @DisplayName("Should validate employees with age > 18")
    public void testMoreThan18() {
        final EmployeeValidationResult validationResult = maximumUnderagePerSectorValidator.validate(legalAgeEmployee);

        assertTrue(validationResult.isValid());
    }

    @Test
    @DisplayName("Should validate employees with age < 18 if the sector would contain < 20% of underage employees")
    public void testLessThan18AndSectorWithLessThan20PercentOfLoad() {
        final var load = new LoadByAge(10L, 100L);
        when(loadService.getUnderageLoad(underAgeEmployee.getSector())).thenReturn(load);

        final EmployeeValidationResult validationResult = maximumUnderagePerSectorValidator.validate(underAgeEmployee);

        assertTrue(validationResult.isValid());
    }

    @Test
    @DisplayName("Should invalidate employees with age > 18 if the sector would contain > 20% of underage employee")
    public void testLessThan18AndSectorWithMoreThan20PercentOfLoad() {
        final var load = new LoadByAge(19L, 98L);
        when(loadService.getUnderageLoad(underAgeEmployee.getSector())).thenReturn(load);

        final EmployeeValidationResult validationResult = maximumUnderagePerSectorValidator.validate(underAgeEmployee);

        assertFalse(validationResult.isValid());
    }

    @Test
    @DisplayName("Should inform a reason when the employee is invalidated")
    public void testValidationMessage() {
        final var load = new LoadByAge(19L, 94L);
        when(loadService.getUnderageLoad(underAgeEmployee.getSector())).thenReturn(load);

        final EmployeeValidationResult validationResult = maximumUnderagePerSectorValidator.validate(underAgeEmployee);

        final String expectedMessage = "Employee age is less than 18 and the sector load would be 21.05%";
        assertTrue(validationResult.getMessages().size() == 1);
        assertEquals(expectedMessage, validationResult.getMessages().get(0));
    }

    private Employee createLegalAgeEmployee() {
        final var personalInformation = new PersonalInformationBuilder()
                .legalAge()
                .build();

        return createEmployee(personalInformation);
    }

    private Employee createUnderAgeEmployee() {
        final var personalInformation = new PersonalInformationBuilder()
                .underage()
                .build();

        return createEmployee(personalInformation);
    }

    private Employee createEmployee(PersonalInformation personalInformation) {
        return new EmployeeBuilder().with(personalInformation).build();
    }
}
