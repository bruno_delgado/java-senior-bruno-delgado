package br.com.mobicare.BackendSeniorBrunoDelgado.domain.services;

import br.com.mobicare.BackendSeniorBrunoDelgado.ContextConfigurationForTests;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.PersonalInformation;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.SectorRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.PersonalInformationBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.SectorBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@AutoConfigureTestDatabase
@Import(ContextConfigurationForTests.class)
@ActiveProfiles({"itests", "load-service-test"})
@DisplayName("Test Cases for Employee Repository")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class LoadServiceImplTest {

    @Autowired
    private SectorRepository sectorRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private LoadService loadService;

    private Sector sector1;
    private Sector sector2;

    @BeforeEach
    public void setUp() {
        sector1 = new SectorBuilder().with("Software Engineering").build();
        sectorRepository.save(sector1);

        sector2 = new SectorBuilder().with("Software Architecture").build();
        sectorRepository.save(sector2);

        Employee employee;
        for (int i = 0; i < 15; i++) {
            employee = createLegalAgeEmployee(sector1);
            employeeRepository.save(employee);
        }

        employee = createUnderageEmployee(sector1);
        employeeRepository.save(employee);

        employee = createElderlyEmployee(sector2);
        employeeRepository.save(employee);
    }

    @Test
    @DisplayName("Should get the underage load of the sector")
    public void testGetUnderageLoad() {
        final var underageLoad = loadService.getUnderageLoad(sector1);

        Assertions.assertEquals(0.0625, underageLoad.getPercentage());
    }

    @Test
    @DisplayName("Should get the elderly load of the sector")
    public void testGetElderlyLoad() {
        final var elderlyLoad = loadService.getElderlyLoad();

        Assertions.assertEquals(0.058823529411764705, elderlyLoad.getPercentage());
    }

    private Employee createLegalAgeEmployee(Sector sector) {
        final var personalInformation = new PersonalInformationBuilder()
                .legalAge()
                .build();

        return createEmployee(personalInformation, sector);
    }

    private Employee createElderlyEmployee(Sector sector) {
        final var personalInformation = new PersonalInformationBuilder()
                .elderly()
                .build();

        return createEmployee(personalInformation, sector);
    }

    private Employee createUnderageEmployee(Sector sector) {
        final var personalInformation = new PersonalInformationBuilder()
                .underage()
                .build();

        return createEmployee(personalInformation, sector);
    }

    private Employee createEmployee(PersonalInformation personalInformation, Sector sector) {
        return new EmployeeBuilder().with(personalInformation).with(sector).build();
    }
}
