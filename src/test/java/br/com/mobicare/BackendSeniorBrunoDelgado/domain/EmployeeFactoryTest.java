package br.com.mobicare.BackendSeniorBrunoDelgado.domain;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.*;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.BlacklistService;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.BlacklistValidator;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.EmployeeValidators;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.ValidationException;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.ContactInformationBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.PersonalInformationBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.SectorBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Test Cases for Employee Factory")
public class EmployeeFactoryTest {

    @Mock
    private BlacklistService blacklistService;

    private Sector sector;
    private EmployeeValidators employeeValidators;
    private ContactInformation contactInformation;
    private PersonalInformation personalInformation;

    @BeforeEach
    public void setUp() {
        sector = new SectorBuilder().build();
        contactInformation = new ContactInformationBuilder().build();
        personalInformation = new PersonalInformationBuilder().build();

        final var blacklistValidator = new BlacklistValidator(blacklistService);
        employeeValidators = new EmployeeValidators(List.of(blacklistValidator));
    }

    @Test
    @DisplayName("Should create an employee")
    public void testCreateEmployee() throws Exception {
        Mockito.when(blacklistService.verifyEmployee(Mockito.any(Employee.class))).thenReturn(Blacklist.ALLOWED);

        final var employee = new EmployeeFactory(employeeValidators)
                .createEmployee(personalInformation, contactInformation, sector);

        assertNotNull(employee);
        assertEquals(sector, employee.getSector());
        assertEquals(personalInformation, employee.getPersonalInformation());
    }

    @Test
    @DisplayName("Should trigger a validation exception if the employee doesnt attend criteries")
    public void testValidations() {
        Mockito.when(blacklistService.verifyEmployee(Mockito.any(Employee.class))).thenReturn(Blacklist.DENIED);

        ValidationException validationException = assertThrows(ValidationException.class, () -> {
            new EmployeeFactory(employeeValidators)
                    .createEmployee(personalInformation, contactInformation, sector);
        });

        String expectedMessage = "Employee is in the Blacklist";
        Assertions.assertEquals(expectedMessage, validationException.getMessage());
    }
}
