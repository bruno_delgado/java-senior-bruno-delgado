package br.com.mobicare.BackendSeniorBrunoDelgado.domain;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.DateOfBirth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Period;

@DisplayName("Test Cases for Date of Birth")
public class DateOfBirthTest {

    @Test
    @DisplayName("Should calculate the age")
    public void testAge() {
        final var today = LocalDate.now();
        final var born = LocalDate.of(1988, 11, 30);
        final int expectedAge = Period.between(born, today).getYears();

        final var dateOfBirth = new DateOfBirth(1988, 11, 30);

        Assertions.assertEquals(expectedAge, dateOfBirth.calculateAge());
    }
}
