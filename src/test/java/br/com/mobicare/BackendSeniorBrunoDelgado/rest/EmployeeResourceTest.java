package br.com.mobicare.BackendSeniorBrunoDelgado.rest;


import br.com.mobicare.BackendSeniorBrunoDelgado.application.dto.AddEmployeeDto;
import br.com.mobicare.BackendSeniorBrunoDelgado.application.dto.QueryEmployeeDto;
import br.com.mobicare.BackendSeniorBrunoDelgado.application.dto.QueryEmployeesDto;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Blacklist;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.SectorRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.BlacklistService;
import br.com.mobicare.BackendSeniorBrunoDelgado.ContextConfigurationForTests;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.SectorBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ActiveProfiles("itests")
@AutoConfigureTestDatabase
@Import(ContextConfigurationForTests.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@DisplayName("Test Cases for Employee Resource")
public class EmployeeResourceTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private SectorRepository sectorRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private BlacklistService blacklistService;

    private Sector sector;
    private AddEmployeeDto addEmployeeDto;

    @BeforeEach
    public void setUp() {
        sector = new SectorBuilder().build();
        sectorRepository.save(sector);

        addEmployeeDto = new AddEmployeeDto();
        addEmployeeDto.setSectorId(sector.getId());
        addEmployeeDto.setCpf("07143298000");
        addEmployeeDto.setName("Bruno Delgado");
        addEmployeeDto.setDateOfBirth(LocalDate.of(1988, 11, 30));
        addEmployeeDto.setPhone("067999998888");
        addEmployeeDto.setEmail("brunodelgado@mail.com");
    }

    @Test
    @DisplayName("Should add a new employee")
    public void testAddEmployee() {
        final ResponseEntity<String> response = restTemplate.postForEntity("/employees", addEmployeeDto, String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getHeaders().get("Location").get(0)).isEqualTo("http://localhost:8080/employees/1");
    }

    @Test
    @DisplayName("Should return error messages when the employee creation was invalid")
    public void testInvalidation() {
        when(blacklistService.verifyEmployee(any(Employee.class))).thenReturn(Blacklist.DENIED);

        final ResponseEntity<ErrorResponse> response = restTemplate.postForEntity("/employees", addEmployeeDto, ErrorResponse.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getMessage()).isEqualTo("Employee is in the Blacklist");
    }

    @Test
    @DisplayName("Should delete an employee")
    public void testDeleteEmployee() {
        final var employee = new EmployeeBuilder().with(sector).build();
        employeeRepository.save(employee);
        final var employeeId = employee.getId();

        final ResponseEntity<String> response = restTemplate
                .exchange("/employees/{id}", HttpMethod.DELETE, null, String.class, employeeId);

        final var deletedEmployee = employeeRepository.findById(employeeId);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(deletedEmployee).isNotPresent();
    }

    @Test
    @DisplayName("Should return an error message when user tries to delete an absent employee")
    public void testDeleteAbsentEmployee() {
        final ResponseEntity<ErrorResponse> response = restTemplate
                .exchange("/employees/{id}", HttpMethod.DELETE, null, ErrorResponse.class, 50L);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getMessage()).isEqualTo("Could not find an employee with id 50.");
    }

    @Test
    @DisplayName("Should query an employee by its id")
    public void testQueryEmployee() {
        final var employee = new EmployeeBuilder().with(sector).build();
        employeeRepository.save(employee);
        final var employeeId = employee.getId();

        final ResponseEntity<QueryEmployeeDto> response = restTemplate.getForEntity("/employees/{id}", QueryEmployeeDto.class, employeeId);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getId()).isEqualTo(employee.getId());
        assertThat(response.getBody().getName()).isEqualTo(employee.getName());
        assertThat(response.getBody().getEmail()).isEqualTo(employee.getEmail());
        assertThat(response.getBody().getSectorId()).isEqualTo(employee.getSector().getId());
        assertThat(response.getBody().getSectorName()).isEqualTo(employee.getSector().getName());
        assertThat(response.getBody().getAge()).isEqualTo(employee.getDateOfBirth().calculateAge());
    }

    @Test
    @DisplayName("Should return an error message when user tries to find an absent employee")
    public void testFindAbsentEmployee() {
        final ResponseEntity<ErrorResponse> response = restTemplate.getForEntity("/employees/{id}", ErrorResponse.class, 50L);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody().getMessage()).isEqualTo("Could not find an employee with id 50.");
    }

    @Test
    @DisplayName("Should return an error message when user tries to add an absent sector")
    public void testAddWithAbsentSector() {
        addEmployeeDto.setSectorId(10L);
        final ResponseEntity<ErrorResponse> response = restTemplate
                .postForEntity("/employees", addEmployeeDto, ErrorResponse.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getMessage()).isEqualTo("The sector 10 was not found.");
    }

    @Test
    @DisplayName("Should query all employees paginates")
    public void testFindAllEmployees() {
        for (var i = 0; i < 20; i++) {
            Employee employee = new EmployeeBuilder().with(sector).build();
            employeeRepository.save(employee);
        }

        final ResponseEntity<QueryEmployeesDto> response = restTemplate
                .getForEntity("/employees?page={page}", QueryEmployeesDto.class, 0);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getTotalPages()).isEqualTo(2);
        assertThat(response.getBody().getActualPage()).isEqualTo(0);
        assertThat(response.getBody().getEmployees().size()).isEqualTo(10);
    }
}
