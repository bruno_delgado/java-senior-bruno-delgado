package br.com.mobicare.BackendSeniorBrunoDelgado.infrastructure;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.SectorRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class InitializeSectorsTest {

    @Mock
    private SectorRepository sectorRepository;

    @Test
    @DisplayName("Should initialize the application with 3 sectors")
    public void testInitialization() throws Exception {
        new InitializeSectors(sectorRepository).afterPropertiesSet();

        Mockito
                .verify(sectorRepository, times(1))
                .saveAll(Mockito.any(List.class));
    }

    @Test
    @DisplayName("Should not save new sectors when there's already one is the database")
    public void testAlreadyInitialized() throws Exception {
        Mockito.when(sectorRepository.count()).thenReturn(1L);

        new InitializeSectors(sectorRepository).afterPropertiesSet();

        Mockito
                .verify(sectorRepository, times(0))
                .saveAll(Mockito.any(List.class));
    }

}
