package br.com.mobicare.BackendSeniorBrunoDelgado.infrastructure;

import br.com.mobicare.BackendSeniorBrunoDelgado.ContextConfigurationForTests;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Blacklist;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.BlacklistService;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.PersonalInformationBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@AutoConfigureTestDatabase
@Import(ContextConfigurationForTests.class)
@ActiveProfiles({"itests", "blacklist-test"})
@DisplayName("Test Cases for Blacklist Service Implementation")
public class BlacklistServiceImplTest {

    @Autowired
    private BlacklistService blacklistService;

    @Test
    @DisplayName("Should be ALLOWED if the cpf is not blacklisted")
    public void testAllowed() {
        final var employee = new EmployeeBuilder().build();

        final Blacklist blacklist = blacklistService.verifyEmployee(employee);

        Assertions.assertEquals(Blacklist.ALLOWED, blacklist);
    }

    @Test
    @DisplayName("Should be DENIED if the cpf is blacklisted")
    public void testDenied() {
        final var cpf = "77386499094";
        final var personalInformation = new PersonalInformationBuilder().withCpf(cpf).build();
        final var employee = new EmployeeBuilder().with(personalInformation).build();

        final Blacklist blacklist = blacklistService.verifyEmployee(employee);

        Assertions.assertEquals(Blacklist.DENIED, blacklist);
    }
}
