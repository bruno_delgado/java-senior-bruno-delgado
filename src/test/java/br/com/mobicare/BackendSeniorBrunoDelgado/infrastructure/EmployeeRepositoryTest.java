package br.com.mobicare.BackendSeniorBrunoDelgado.infrastructure;

import br.com.mobicare.BackendSeniorBrunoDelgado.ContextConfigurationForTests;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.PersonalInformation;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.SectorRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.PersonalInformationBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.SectorBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureTestDatabase
@ActiveProfiles({"itests"})
@Import(ContextConfigurationForTests.class)
@DisplayName("Test Cases for Employee Repository")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SectorRepository sectorRepository;

    private Sector sector1;
    private Sector sector2;
    private int totalElderly;
    private int totalUnderage;

    @BeforeEach
    public void setUp() {
        sector1 = new SectorBuilder().with("Software Engineering").build();
        sectorRepository.save(sector1);

        sector2 = new SectorBuilder().with("Software Architecture").build();
        sectorRepository.save(sector2);

        totalUnderage = 40;
        totalElderly = 5;

        Employee employee;
        for (int i = 0; i < totalUnderage; i++) {
            employee = createUnderAgeEmployee(sector1);
            employeeRepository.save(employee);
        }

        for (int i = 0; i < totalElderly; i++) {
            employee = createElderlyAgeEmployee(sector2);
            employeeRepository.save(employee);
        }
    }

    @Test
    @DisplayName("Should count all the employees in the sector (elderly)")
    public void testCountBySector() {
        final var countBySector = employeeRepository.countAllBySector_Id(sector1.getId());

        assertEquals(totalUnderage, countBySector);
    }

    @Test
    @DisplayName("Should count all the employees with date after specified (underage)")
    public void testCountByAfterDate() {
        LocalDate localDate = LocalDate.now();
        int eighteenYearsAgo = localDate.getYear() - 19;
        LocalDate underage = LocalDate.of(eighteenYearsAgo, localDate.getMonth(), localDate.getDayOfMonth());

        final var countBySectorAndAge = employeeRepository
                .countAllByPersonalInformation_DateOfBirth_DateAfterAndSector_Id(underage, sector1.getId());

        assertEquals(totalUnderage, countBySectorAndAge);
    }

    @Test
    @DisplayName("Should count for all the employees with date before specified (elderly)")
    public void testCountByBeforeDate() {
        LocalDate localDate = LocalDate.now();
        int eighteenYearsAgo = localDate.getYear() - 65;
        LocalDate elderly = LocalDate.of(eighteenYearsAgo, localDate.getMonth(), localDate.getDayOfMonth());

        final var countBySectorAndAge = employeeRepository.countAllByPersonalInformation_DateOfBirth_DateBefore(elderly);

        assertEquals(totalElderly, countBySectorAndAge);
    }

    @Test
    @DisplayName("Should count for all the company employees with date before specified (elderly)")
    public void testCountAll() {
        final var countBySectorAndAge = employeeRepository.count();

        assertEquals(totalElderly + totalUnderage, countBySectorAndAge);
    }

    @Test
    @DisplayName("Should the employees paginated")
    public void testQueryPaginated() {
        final var size = 5;
        final var pageable = PageRequest.of(0, size).withSort(Sort.by("id"));

        final var result = employeeRepository.findAllGroupedBySector(pageable);

        final var expectedNumberOfPages = Math.ceil((totalElderly + totalUnderage) / 5);
        assertEquals(size, result.getNumberOfElements());
        assertEquals(expectedNumberOfPages, result.getTotalPages());
    }

    private Employee createElderlyAgeEmployee(Sector sector) {
        final var personalInformation = new PersonalInformationBuilder()
                .elderly()
                .build();
        return createEmployee(personalInformation, sector);
    }

    private Employee createUnderAgeEmployee(Sector sector) {
        final var personalInformation = new PersonalInformationBuilder().underage().build();
        return createEmployee(personalInformation, sector);
    }

    private Employee createEmployee(PersonalInformation personalInformation, Sector sector) {
        return new EmployeeBuilder().with(personalInformation).with(sector).build();
    }
}
