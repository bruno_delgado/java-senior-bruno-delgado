package br.com.mobicare.BackendSeniorBrunoDelgado.application;

import br.com.mobicare.BackendSeniorBrunoDelgado.application.dto.AddEmployeeDto;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.*;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.SectorRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.BlacklistService;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.ValidationException;
import br.com.mobicare.BackendSeniorBrunoDelgado.ContextConfigurationForTests;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.SectorBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@Transactional
@SpringBootTest
@ActiveProfiles("itests")
@AutoConfigureTestDatabase
@Import(ContextConfigurationForTests.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@DisplayName("Test Cases for Employee Application")
public class EmployeeApplicationTest {

    @Autowired
    private EmployeeApplication employeeApplication;

    @Autowired
    private SectorRepository sectorRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private BlacklistService blacklistService;

    private Sector sector;

    private AddEmployeeDto addEmployeeDto;

    private Employee employee;

    @BeforeEach
    public void setUp() {
        sector = new SectorBuilder().build();
        sectorRepository.save(sector);

        addEmployeeDto = new AddEmployeeDto();
        addEmployeeDto.setSectorId(sector.getId());
        addEmployeeDto.setCpf("07143298000");
        addEmployeeDto.setName("Bruno Delgado");
        addEmployeeDto.setDateOfBirth(LocalDate.of(1988, 11, 30));
        addEmployeeDto.setEmail("brunodelgado@mail.com");
        addEmployeeDto.setPhone("067999995555");

        when(blacklistService.verifyEmployee(any(Employee.class))).thenReturn(Blacklist.ALLOWED);
    }

    @Test
    @DisplayName("Should create and save a new Employee")
    public void testEmployee() throws Exception {
        final Long id = employeeApplication.addEmployee(addEmployeeDto);

        final var savedEmployee = employeeRepository.findByPersonalInformation_Cpf(addEmployeeDto.getCpf());
        assertNotNull(savedEmployee);
        assertEquals(savedEmployee.getId(), id);
    }

    @Test
    @DisplayName("Should map the employee fields correctly")
    public void testMapping() throws Exception {
        employeeApplication.addEmployee(addEmployeeDto);

        final var savedEmployee = employeeRepository.findByPersonalInformation_Cpf(addEmployeeDto.getCpf());
        assertEquals(addEmployeeDto.getSectorId(), savedEmployee.getSector().getId());
        assertEquals(addEmployeeDto.getName(), savedEmployee.getName());
        assertEquals(addEmployeeDto.getCpf(), savedEmployee.getCpf());
        assertEquals(addEmployeeDto.getDateOfBirth(), savedEmployee.getDateOfBirth().getLocalDate());
        assertEquals(addEmployeeDto.getEmail(), savedEmployee.getEmail());
        assertEquals(addEmployeeDto.getPhone(), savedEmployee.getPhone());
    }

    @Test
    @DisplayName("Should not save the employee when some validation fail")
    public void testValidation() {
        when(blacklistService.verifyEmployee(any(Employee.class))).thenReturn(Blacklist.DENIED);

        ValidationException validationException = assertThrows(ValidationException.class, () -> {
            employeeApplication.addEmployee(addEmployeeDto);
        });

        String expectedMessage = "Employee is in the Blacklist";
        assertEquals(expectedMessage, validationException.getMessage());
    }

    @Test
    @DisplayName("Should remove an employee")
    public void testRemove() throws Exception {
        employee = new EmployeeBuilder().with(sector).build();
        employeeRepository.save(employee);
        final var employeeId = employee.getId();

        employeeApplication.remove(employeeId);

        final var employeeFound = employeeRepository.findById(employeeId);
        assertFalse(employeeFound.isPresent());
    }

    @Test
    @DisplayName("Should throw an exception when trying to remove an absent employee")
    public void testRemoveUnexistingEmployee() {
        final var employeeId = 1L;

        final EmployeeNotFoundException employeeNotFoundException = assertThrows(EmployeeNotFoundException.class, () -> {
            employeeApplication.remove(employeeId);
        });

        final var expectedMessage = "Could not find an employee with id 1.";
        Assertions.assertEquals(expectedMessage, employeeNotFoundException.getMessage());
    }

    @Test
    @DisplayName("Should throw an exception when trying to add employee in an absent sector")
    public void testAddInAbsentSector() {
        addEmployeeDto.setSectorId(10L);

        final SectorNotFoundException employeeNotFoundException = assertThrows(SectorNotFoundException.class, () -> {
            employeeApplication.addEmployee(addEmployeeDto);
        });

        final var expectedMessage = "The sector 10 was not found.";
        Assertions.assertEquals(expectedMessage, employeeNotFoundException.getMessage());
    }
}
