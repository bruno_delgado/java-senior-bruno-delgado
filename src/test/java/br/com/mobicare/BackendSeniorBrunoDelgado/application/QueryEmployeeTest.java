package br.com.mobicare.BackendSeniorBrunoDelgado.application;

import br.com.mobicare.BackendSeniorBrunoDelgado.ContextConfigurationForTests;
import br.com.mobicare.BackendSeniorBrunoDelgado.application.dto.QueryEmployeesDto;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.EmployeeNotFoundException;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.SectorRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.EmployeeBuilder;
import br.com.mobicare.BackendSeniorBrunoDelgado.utils.SectorBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional
@SpringBootTest
@ActiveProfiles("itests")
@AutoConfigureTestDatabase
@Import(ContextConfigurationForTests.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@DisplayName("Test Cases for Query Employees")
public class QueryEmployeeTest {

    @Autowired
    private QueryEmployees queryEmployees;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SectorRepository sectorRepository;

    private Sector sector;

    @BeforeEach
    public void setUp() {
        sector = new SectorBuilder().build();
        sectorRepository.save(sector);
    }

    @Test
    @DisplayName("Should query an employee by its id")
    public void testQueryById() throws Exception {
        final var employee = new EmployeeBuilder().with(sector).build();
        employeeRepository.save(employee);
        final var employeeId = employee.getId();

        final var queryEmployeesDto = queryEmployees.getById(employeeId);

        assertEquals(employee.getId(), queryEmployeesDto.getId());
        assertEquals(employee.getName(), queryEmployeesDto.getName());
        assertEquals(employee.getEmail(), queryEmployeesDto.getEmail());
        assertEquals(employee.getSector().getId(), queryEmployeesDto.getSectorId());
        assertEquals(employee.getSector().getName(), queryEmployeesDto.getSectorName());
        assertEquals(employee.getDateOfBirth().calculateAge(), queryEmployeesDto.getAge());
    }

    @Test
    @DisplayName("Should throw an exception when trying to get an absent employee")
    public void testRemoveUnexistingEmployee() {
        final var employeeId = 1L;

        final EmployeeNotFoundException employeeNotFoundException = assertThrows(EmployeeNotFoundException.class, () -> {
            queryEmployees.getById(employeeId);
        });

        final var expectedMessage = "Could not find an employee with id 1.";
        assertEquals(expectedMessage, employeeNotFoundException.getMessage());
    }

    @Test
    @DisplayName("Should query all the employees paginated in a limit of 10 values")
    public void testQueryPaginated() {
        final var page = 0;
        persistEmployees(20);

        final QueryEmployeesDto queryEmployeesAll = queryEmployees.getAll(page);

        assertEquals(10, queryEmployeesAll.getEmployees().size());
        assertEquals(2, queryEmployeesAll.getTotalPages());
        assertEquals(0, queryEmployeesAll.getActualPage());
    }

    private void persistEmployees(int quantity) {
        for (var i = 0; i < quantity; i++) {
            final var employee = new EmployeeBuilder().with(sector).build();
            employeeRepository.save(employee);
        }
    }
}
