package br.com.mobicare.BackendSeniorBrunoDelgado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendSeniorBrunoDelgadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendSeniorBrunoDelgadoApplication.class, args);
	}

}
