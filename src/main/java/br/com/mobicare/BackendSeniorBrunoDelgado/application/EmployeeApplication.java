package br.com.mobicare.BackendSeniorBrunoDelgado.application;

import br.com.mobicare.BackendSeniorBrunoDelgado.application.dto.AddEmployeeDto;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.*;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.SectorRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.ValidationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class EmployeeApplication {

    private final EmployeeFactory employeeFactory;
    private final SectorRepository sectorRepository;
    private final EmployeeRepository employeeRepository;

    public EmployeeApplication(EmployeeFactory employeeFactory, EmployeeRepository employeeRepository,
                               SectorRepository sectorRepository) {
        this.employeeFactory = employeeFactory;
        this.sectorRepository = sectorRepository;
        this.employeeRepository = employeeRepository;
    }

    @Transactional
    public Long addEmployee(AddEmployeeDto addEmployeeDto) throws ValidationException, SectorNotFoundException {
        final var isPresent = sectorRepository.existsById(addEmployeeDto.getSectorId());
        if (!isPresent) {
            throw new SectorNotFoundException(addEmployeeDto.getSectorId());
        }

        final Sector sector = sectorRepository.getById(addEmployeeDto.getSectorId());

        final var contactInformation = getContactInformation(addEmployeeDto);
        final var personalInformation = getPersonalInformation(addEmployeeDto);
        final var employee = employeeFactory.createEmployee(personalInformation, contactInformation, sector);

        final var savedEmployee = employeeRepository.save(employee);
        return savedEmployee.getId();
    }

    public void remove(Long employeeId) throws EmployeeNotFoundException {
        try {
            employeeRepository.deleteById(employeeId);
        } catch (EmptyResultDataAccessException exception) {
            throw new EmployeeNotFoundException(employeeId);
        }
    }

    private ContactInformation getContactInformation(AddEmployeeDto addEmployeeDto) {
        return new ContactInformation(addEmployeeDto.getEmail(), addEmployeeDto.getPhone());
    }

    private PersonalInformation getPersonalInformation(AddEmployeeDto addEmployeeDto) {
        final var year = addEmployeeDto.getDateOfBirth().getYear();
        final var day = addEmployeeDto.getDateOfBirth().getDayOfMonth();
        final var month = addEmployeeDto.getDateOfBirth().getMonth();
        final var dateOfBirth = new DateOfBirth(year, month.getValue(), day);

        return new PersonalInformation(addEmployeeDto.getCpf(), addEmployeeDto.getName(), dateOfBirth);
    }
}
