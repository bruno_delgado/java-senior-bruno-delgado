package br.com.mobicare.BackendSeniorBrunoDelgado.application.dto;

import java.util.List;

public class QueryEmployeesDto {
    private int actualPage;
    private int totalPages;
    private List<QueryEmployeeDto> employees;

    public QueryEmployeesDto() {
    }

    public QueryEmployeesDto(int actualPage, int totalPages, List<QueryEmployeeDto> employees) {
        this.actualPage = actualPage;
        this.totalPages = totalPages;
        this.employees = employees;
    }

    public int getActualPage() {
        return actualPage;
    }

    public void setActualPage(int actualPage) {
        this.actualPage = actualPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<QueryEmployeeDto> getEmployees() {
        return employees;
    }

    public void setEmployees(List<QueryEmployeeDto> employees) {
        this.employees = employees;
    }
}
