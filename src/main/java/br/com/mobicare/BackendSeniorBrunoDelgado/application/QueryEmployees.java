package br.com.mobicare.BackendSeniorBrunoDelgado.application;

import br.com.mobicare.BackendSeniorBrunoDelgado.application.dto.QueryEmployeeDto;
import br.com.mobicare.BackendSeniorBrunoDelgado.application.dto.QueryEmployeesDto;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.EmployeeNotFoundException;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QueryEmployees {

    private static final int QUERY_LIMIT = 10;

    private EmployeeRepository employeeRepository;

    public QueryEmployees(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public QueryEmployeeDto getById(Long id) throws EmployeeNotFoundException {
        try {
            final var employee = employeeRepository.getById(id);

            final var queryEmployeesDto = convert(employee);

            return queryEmployeesDto;
        } catch (EntityNotFoundException exception) {
            throw new EmployeeNotFoundException(id);
        }
    }

    private QueryEmployeeDto convert(Employee employee) {
        QueryEmployeeDto queryEmployeeDto = new QueryEmployeeDto();
        queryEmployeeDto.setId(employee.getId());
        queryEmployeeDto.setName(employee.getName());
        queryEmployeeDto.setEmail(employee.getEmail());
        queryEmployeeDto.setSectorId(employee.getSector().getId());
        queryEmployeeDto.setSectorName(employee.getSector().getName());
        queryEmployeeDto.setAge(employee.getDateOfBirth().calculateAge());

        return queryEmployeeDto;
    }

    public QueryEmployeesDto getAll(int page) {
        final var pageRequest = PageRequest.of(page, QUERY_LIMIT).withSort(Sort.by("id"));
        final var employees = employeeRepository.findAllGroupedBySector(pageRequest);

        final var actualPage = employees.getNumber();
        final var totalPages = employees.getTotalPages();
        final List<QueryEmployeeDto> queryEmployeeDtos = employees.get().map(this::convert).collect(Collectors.toList());

        final QueryEmployeesDto queryEmployeesDto = new QueryEmployeesDto(actualPage, totalPages, queryEmployeeDtos);
        return queryEmployeesDto;
    }
}
