package br.com.mobicare.BackendSeniorBrunoDelgado.application.dto;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class AddEmployeeDto {

    @NotNull( message = "It is mandatory to set the sectorId field")
    private Long sectorId;

    @Pattern( regexp = "^[0-9]{11}$", message = "Invalid CPF format. Please use only 11 numbers")
    private String cpf;

    @Size(min = 5, message = "The field name is to small. Please insert employee's full name")
    @NotEmpty( message = "It is mandatory to set the employee name")
    private String name;

    @NotNull( message = "It is mandatory to set the employee date of birth")
    private LocalDate dateOfBirth;

    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    @Pattern( regexp = "^[0-9]{12}", message = "Invalid phone number. Please use the pattern DDDPPPPPSSSS")
    private String phone;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getSectorId() {
        return sectorId;
    }

    public void setSectorId(Long sectorId) {
        this.sectorId = sectorId;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
