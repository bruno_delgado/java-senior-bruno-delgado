package br.com.mobicare.BackendSeniorBrunoDelgado.rest;

import br.com.mobicare.BackendSeniorBrunoDelgado.application.EmployeeApplication;
import br.com.mobicare.BackendSeniorBrunoDelgado.application.QueryEmployees;
import br.com.mobicare.BackendSeniorBrunoDelgado.application.dto.AddEmployeeDto;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.EmployeeNotFoundException;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.SectorNotFoundException;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("employees")
public class EmployeeResource {

    private final QueryEmployees queryEmployees;
    private final EmployeeApplication employeeApplication;

    public EmployeeResource(EmployeeApplication employeeApplication, QueryEmployees queryEmployees) {
        this.employeeApplication = employeeApplication;
        this.queryEmployees = queryEmployees;
    }

    @GetMapping
    public ResponseEntity get(@RequestParam(name = "page", defaultValue = "0") Integer page) {
        final var queryEmployeesDto = queryEmployees.getAll(page);
        return ResponseEntity.ok().body(queryEmployeesDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable("id") Long employeeId) {
        ResponseEntity responseEntity = null;

        try {
            final var queryEmployeesDto = queryEmployees.getById(employeeId);
            responseEntity = ResponseEntity.ok().body(queryEmployeesDto);
        } catch (EmployeeNotFoundException exception) {
            final var errorResponse = new ErrorResponse(exception.getMessage());
            responseEntity = new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
        }

        return responseEntity;
    }

    @PostMapping
    public ResponseEntity add(@Valid @RequestBody AddEmployeeDto addEmployeeDto, BindingResult bindingResult) {
        ResponseEntity responseEntity = null;

        try {
            check(bindingResult);

            final Long id = employeeApplication.addEmployee(addEmployeeDto);

            final URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(id)
                    .toUri();

            responseEntity = ResponseEntity.created(location).build();
        } catch (ValidationException | SectorNotFoundException exception) {
            final var errorResponse = new ErrorResponse(exception.getMessage());
            responseEntity = new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
        } finally {
            return responseEntity;
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long employeeId) {
        ResponseEntity responseEntity = null;

        try {
            employeeApplication.remove(employeeId);
            responseEntity = ResponseEntity.ok().build();
        } catch (Exception exception) {
            final var errorResponse = new ErrorResponse(exception.getMessage());
            responseEntity = new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
        }

        return responseEntity;
    }

    private void check(BindingResult bindingResult) throws ValidationException {
        if (bindingResult.hasErrors()) {
            final List<String> messages = bindingResult
                    .getFieldErrors().stream()
                    .map(FieldError::getDefaultMessage)
                    .collect(Collectors.toList());
            throw new ValidationException(messages);
        }
    }
}

