package br.com.mobicare.BackendSeniorBrunoDelgado.rest.configurations;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.vendor.Database;

import javax.sql.DataSource;

@Configuration
@Profile("!itests")
public class DataSourceConfiguration {

    @Bean
    public DataSource dataSource(Environment environment) {
        var driver = environment.getProperty("database.driver");
        var username = environment.getProperty("database.username");
        var password = environment.getProperty("database.password");
        var dataBaseConnectionString = environment.getProperty("database.url");

        var dataSource = new BasicDataSource();
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(dataBaseConnectionString);

        return dataSource;
    }

    @Bean
    public Database database() {
        return Database.POSTGRESQL;
    }
}
