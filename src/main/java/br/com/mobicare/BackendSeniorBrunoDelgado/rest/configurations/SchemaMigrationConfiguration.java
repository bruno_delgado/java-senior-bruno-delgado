package br.com.mobicare.BackendSeniorBrunoDelgado.rest.configurations;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
public class SchemaMigrationConfiguration {

    @Bean(initMethod = "migrate")
    public Flyway flyway(DataSource dataSource, Environment environment) {
        FluentConfiguration configuration = Flyway.configure();
        configuration.schemas("public");
        configuration.dataSource(dataSource);
        return configuration.load();
    }
}
