package br.com.mobicare.BackendSeniorBrunoDelgado.rest.configurations;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder()
                .rootUri("https://5e74cb4b9dff120016353b04.mockapi.io/api/v1/blacklist")
                .build();
    }
}
