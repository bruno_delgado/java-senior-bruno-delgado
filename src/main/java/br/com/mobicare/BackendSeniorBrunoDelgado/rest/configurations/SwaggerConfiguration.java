package br.com.mobicare.BackendSeniorBrunoDelgado.rest.configurations;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(info = @Info(title = "Technical Avaliation", version = "1.0.0", description = "Technical Avaliation for Senior Backend Developer"))
public class SwaggerConfiguration { }
