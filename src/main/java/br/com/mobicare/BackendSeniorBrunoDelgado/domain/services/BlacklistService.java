package br.com.mobicare.BackendSeniorBrunoDelgado.domain.services;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Blacklist;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;

public interface BlacklistService {
    Blacklist verifyEmployee(Employee employee);
}
