package br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectorRepository extends JpaRepository<Sector, Long> {}
