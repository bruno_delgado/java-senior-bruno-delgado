package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

public enum Blacklist {
    ALLOWED,
    DENIED
}
