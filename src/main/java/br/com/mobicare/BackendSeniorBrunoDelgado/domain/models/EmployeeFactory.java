package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.EmployeeValidators;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.ValidationException;
import org.springframework.stereotype.Component;

@Component
public class EmployeeFactory {

    private final EmployeeValidators employeeValidators;

    public EmployeeFactory(EmployeeValidators employeeValidators) {
        this.employeeValidators = employeeValidators;
    }

    public Employee createEmployee(PersonalInformation personalInformation,
                                   ContactInformation contactInformation,
                                   Sector sector) throws ValidationException {
        final var employee = new Employee(personalInformation, contactInformation, sector);
        final var validationResult = employeeValidators.validate(employee);

        if (validationResult.isValid()) {
            return employee;
        } else {
            throw new ValidationException(validationResult.getMessages());
        }
    }
}
