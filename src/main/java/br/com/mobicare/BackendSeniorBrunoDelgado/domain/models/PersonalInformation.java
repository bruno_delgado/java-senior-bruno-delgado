package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
public final class PersonalInformation {

    private final String cpf;
    private final String name;

    @Embedded
    private final DateOfBirth dateOfBirth;

    protected PersonalInformation() {
        this.cpf = null;
        this.name = null;
        this.dateOfBirth = null;
    }

    public PersonalInformation(String cpf, String name, DateOfBirth dateOfBirth) {
        this.cpf = cpf;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    public String getCpf() {
        return cpf;
    }

    public String getName() {
        return name;
    }

    public DateOfBirth getDateOfBirth() {
        return dateOfBirth;
    }

    public int getAge() {
        return dateOfBirth.calculateAge();
    }
}
