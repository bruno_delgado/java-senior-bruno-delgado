package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

import java.util.Objects;

public final class LoadByAge {

    final private Long total;
    final private Long aged;

    public LoadByAge(Long aged, Long total) {
        this.aged = aged;
        this.total = total;
    }

    public double getPercentage() {
        if (total == 0) return total;
        return (double) aged / total;
    }

    public LoadByAge sumUp(int quantity) {
        return new LoadByAge(aged + quantity, total + quantity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoadByAge loadByAge = (LoadByAge) o;
        return Objects.equals(total, loadByAge.total) && Objects.equals(aged, loadByAge.aged);
    }

    @Override
    public int hashCode() {
        return Objects.hash(total, aged);
    }
}