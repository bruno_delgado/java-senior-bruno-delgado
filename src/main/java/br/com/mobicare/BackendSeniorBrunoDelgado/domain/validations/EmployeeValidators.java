package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeValidators implements EmployeeValidation {

    private final List<EmployeeValidation> employeeValidators;

    public EmployeeValidators(List<EmployeeValidation> employeeValidators) {
        this.employeeValidators = employeeValidators;
    }

    @Override
    public EmployeeValidationResult validate(Employee employee) {
        var isValid = true;
        final List<String> messages = new ArrayList<>();

        for (EmployeeValidation employeeValidation : employeeValidators) {
            EmployeeValidationResult validationResult = employeeValidation.validate(employee);

            if (!validationResult.isValid()) {
                isValid = false;
                messages.addAll(validationResult.getMessages());
            }
        }

        return new EmployeeValidationResult(isValid, messages);
    }
}
