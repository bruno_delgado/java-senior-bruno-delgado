package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations;

import java.util.List;

public final class EmployeeValidationResult {

    private final boolean isValid;
    private final List<String> messages;

    public EmployeeValidationResult(boolean isValid, List<String> messages) {
        this.isValid = isValid;
        this.messages = messages;
    }

    public boolean isValid() {
        return isValid;
    }

    public List<String> getMessages() {
        return List.copyOf(messages);
    }
}
