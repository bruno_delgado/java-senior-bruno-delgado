package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Blacklist;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.BlacklistService;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class BlacklistValidator implements EmployeeValidation {

    private final BlacklistService blacklistService;

    public BlacklistValidator(BlacklistService blacklistService) {
        this.blacklistService = blacklistService;
    }

    @Override
    public EmployeeValidationResult validate(Employee employee) {
        var isValid = true;
        String message = null;

        final Blacklist blacklist = blacklistService.verifyEmployee(employee);

        if (Blacklist.DENIED.equals(blacklist)) {
            isValid = false;
            message = "Employee is in the Blacklist";
        }

        return new EmployeeValidationResult(isValid, Arrays.asList(message));
    }

}
