package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class AbsentInSectorValidator implements EmployeeValidation {

    private final EmployeeRepository employeeRepository;

    public AbsentInSectorValidator(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public EmployeeValidationResult validate(Employee employee) {
        var isValid = true;
        String message = null;

        final var employeeFound = employeeRepository.findByPersonalInformation_Cpf(employee.getCpf());

        if (employeeFound != null && employee.getSector() != null) {
            isValid = false;
            message = String.format("Employee already belongs to the sector \"%s\"", employee.getSector());
        }

        return new EmployeeValidationResult(isValid, Arrays.asList(message));
    }
}
