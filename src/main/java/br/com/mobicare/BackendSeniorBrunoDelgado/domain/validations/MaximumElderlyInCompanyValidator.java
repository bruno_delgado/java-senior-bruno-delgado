package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.LoadByAge;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.LoadService;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.Arrays;

@Component
public class MaximumElderlyInCompanyValidator implements EmployeeValidation {

    public static final double MAXIMUM_ALLOWED = 0.2;
    public static final int ELDERLY_AGE = 65;

    final private LoadService loadService;

    public MaximumElderlyInCompanyValidator(LoadService loadService) {
        this.loadService = loadService;
    }

    @Override
    public EmployeeValidationResult validate(Employee employee) {
        var isValid = true;
        String message = null;

        int age = employee.getPersonalInformation().getAge();

        if (age > ELDERLY_AGE) {
            double underageLoad = getLoadPercentage();
            if (underageLoad > MAXIMUM_ALLOWED) {
                isValid = false;
                message = getValidationMessage(underageLoad);
            }
        }

        return new EmployeeValidationResult(isValid, Arrays.asList(message));
    }

    private double getLoadPercentage() {
        LoadByAge oldLoadByAge = this.loadService.getElderlyLoad();
        LoadByAge newLoadByAge = oldLoadByAge.sumUp(1);

        return newLoadByAge.getPercentage();
    }

    private String getValidationMessage(double loadPercentage) {
        DecimalFormat df = new DecimalFormat("#.##");
        String percentage = df.format(loadPercentage * 100);
        return String.format("Employee age is less than 65 and the sector load would be %s%%", percentage);
    }
}
