package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

public class SectorNotFoundException extends Exception {
    public SectorNotFoundException(Long sectorId) {
        super(String.format("The sector %d was not found.", sectorId));
    }
}
