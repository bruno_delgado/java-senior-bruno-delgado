package br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findByPersonalInformation_Cpf(String cpf);

    long countAllBySector_Id(long sectorId);

    long countAllByPersonalInformation_DateOfBirth_DateAfterAndSector_Id(LocalDate date, Long sectorId);

    long countAllByPersonalInformation_DateOfBirth_DateBefore(LocalDate date);

    @Query("SELECT e FROM Employee e GROUP BY e.sector.name, e.id")
    Page<Employee> findAllGroupedBySector(Pageable pageable);
}
