package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
public abstract class GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object object) {
        if(this == object) {
            return true;
        }

        if (object != null && this.getClass().equals(object.getClass())) {
            GenericEntity other = (GenericEntity) object;
            return other.getId() != null && this.id.equals(other.getId());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }
}