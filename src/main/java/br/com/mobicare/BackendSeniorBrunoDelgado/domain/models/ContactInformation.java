package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

import javax.persistence.Embeddable;

@Embeddable
public final class ContactInformation {

    private final String email;
    private final String phone;

    protected ContactInformation() {
        this.email = null;
        this.phone = null;
    }

    public ContactInformation(String email, String phone) {
        this.phone = phone;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
