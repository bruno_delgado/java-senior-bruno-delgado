package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;

public interface EmployeeValidation {
    EmployeeValidationResult validate(Employee employee);
}
