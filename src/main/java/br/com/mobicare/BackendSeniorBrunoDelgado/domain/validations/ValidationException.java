package br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations;

import java.util.List;

public class ValidationException extends Exception {
    public ValidationException(List<String> messages) {
        super(String.join(". ", messages).trim());
    }
}
