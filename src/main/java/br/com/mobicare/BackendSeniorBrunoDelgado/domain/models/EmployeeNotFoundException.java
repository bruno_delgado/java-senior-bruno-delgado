package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

public class EmployeeNotFoundException extends Exception {
    public EmployeeNotFoundException(Long employeeId) {
        super(String.format("Could not find an employee with id %d.", employeeId));
    }
}
