package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

@Embeddable
@Access(AccessType.FIELD)
public final class DateOfBirth {

    @Column(name = "date_of_birth")
    final private LocalDate date;

    protected DateOfBirth() {
        date = null;
    }

    public DateOfBirth(int year, int month, int day) {
        date = LocalDate.of(year, month, day);
    }

    public int calculateAge() {
        final var today = LocalDate.now();
        return Period.between(date, today).getYears();
    }

    public LocalDate getLocalDate() {
        return date;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        DateOfBirth that = (DateOfBirth) other;
        return Objects.equals(date, that.date);
    }
}
