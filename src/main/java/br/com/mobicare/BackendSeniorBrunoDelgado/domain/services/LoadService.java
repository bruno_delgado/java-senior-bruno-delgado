package br.com.mobicare.BackendSeniorBrunoDelgado.domain.services;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.LoadByAge;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;

public interface LoadService {
    LoadByAge getUnderageLoad(Sector sector);

    LoadByAge getElderlyLoad();
}
