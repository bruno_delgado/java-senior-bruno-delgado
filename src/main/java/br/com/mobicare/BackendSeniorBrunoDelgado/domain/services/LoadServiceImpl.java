package br.com.mobicare.BackendSeniorBrunoDelgado.domain.services;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.EmployeeRepository;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.LoadByAge;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

import static br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.MaximumElderlyInCompanyValidator.ELDERLY_AGE;
import static br.com.mobicare.BackendSeniorBrunoDelgado.domain.validations.MaximumUnderagePerSectorValidator.LEGAL_AGE;

@Component
public class LoadServiceImpl implements LoadService {

    private EmployeeRepository employeeRepository;

    public LoadServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public LoadByAge getUnderageLoad(Sector sector) {
        final LocalDate queryDate = getYearBack(LEGAL_AGE);

        final var totalInSector = this.employeeRepository.countAllBySector_Id(sector.getId());
        final var underageCounter = this.employeeRepository
                .countAllByPersonalInformation_DateOfBirth_DateAfterAndSector_Id(queryDate, sector.getId());

        return new LoadByAge(underageCounter, totalInSector);
    }

    @Override
    public LoadByAge getElderlyLoad() {
        final LocalDate queryDate = getYearBack(ELDERLY_AGE);

        final var totalInCompany = this.employeeRepository.count();
        final var underageCounter = this.employeeRepository
                .countAllByPersonalInformation_DateOfBirth_DateBefore(queryDate);

        return new LoadByAge(underageCounter, totalInCompany);
    }

    private LocalDate getYearBack(int years) {
        final var now = LocalDate.now();
        final var oldYear = now.getYear() - years;
        return LocalDate.of(oldYear, now.getMonth(), now.getDayOfMonth());
    }

}
