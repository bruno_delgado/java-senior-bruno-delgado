package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

import javax.persistence.Entity;

@Entity
public class Sector extends GenericEntity {

    private String name;

    protected Sector() {}

    public Sector(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
