package br.com.mobicare.BackendSeniorBrunoDelgado.domain.models;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Employee extends GenericEntity {

    @ManyToOne
    private Sector sector;

    @Embedded
    private PersonalInformation personalInformation;

    @Embedded
    private ContactInformation contactInformation;

    protected Employee() {}

    public Employee(PersonalInformation personalInformation, ContactInformation contactInformation, Sector sector) {
        this.sector = sector;
        this.contactInformation = contactInformation;
        this.personalInformation = personalInformation;
    }

    public String getName() {
        return personalInformation.getName();
    }

    public DateOfBirth getDateOfBirth() {
        return personalInformation.getDateOfBirth();
    }

    public String getCpf() {
        return personalInformation.getCpf();
    }

    public Sector getSector() {
        return sector;
    }

    public PersonalInformation getPersonalInformation() {
        return personalInformation;
    }

    public String getEmail() {
        return contactInformation.getEmail();
    }

    public String getPhone() {
        return contactInformation.getPhone();
    }
}
