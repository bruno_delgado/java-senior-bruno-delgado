package br.com.mobicare.BackendSeniorBrunoDelgado.infrastructure;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Sector;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.repository.SectorRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class InitializeSectors implements InitializingBean {

    private final SectorRepository sectorRepository;

    public InitializeSectors(SectorRepository sectorRepository) {
        this.sectorRepository = sectorRepository;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (sectorRepository.count() == 0) {
            final var finance = new Sector("Finance");
            final var humanResources = new Sector("Human Resources");
            final var softwareDevelopment = new Sector("Software Development");

            sectorRepository.saveAll(Arrays.asList(finance, humanResources, softwareDevelopment));
        }
    }
}
