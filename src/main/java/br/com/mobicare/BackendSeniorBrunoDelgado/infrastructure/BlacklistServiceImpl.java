package br.com.mobicare.BackendSeniorBrunoDelgado.infrastructure;

import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Blacklist;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.models.Employee;
import br.com.mobicare.BackendSeniorBrunoDelgado.domain.services.BlacklistService;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class BlacklistServiceImpl implements BlacklistService {

    public static final String BLACKLIST_PATH = "/?cpf={cpf}";
    private final RestTemplate restTemplate;

    public BlacklistServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Blacklist verifyEmployee(Employee employee) {
        final var response = restTemplate.getForEntity(BLACKLIST_PATH, Object[].class, employee.getCpf());

        if (response.getBody().length > 0) {
            return Blacklist.DENIED;
        }

        return Blacklist.ALLOWED;
    }
}
