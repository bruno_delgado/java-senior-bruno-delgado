create table public.employee
(
    id bigserial constraint employee_pkey primary key,
    cpf varchar(11) NOT NULL,
    date_of_birth date NOT NULL,
    name varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    phone varchar(12) NOT NULL,
    sector_id bigint constraint fk_sector_1 references sector
);