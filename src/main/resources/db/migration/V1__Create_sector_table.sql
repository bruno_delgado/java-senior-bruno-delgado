create table public.sector
(
    id bigserial constraint sector_pkey primary key,
    name varchar(255) NOT NULL
);